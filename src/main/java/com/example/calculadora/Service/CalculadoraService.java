package com.example.calculadora.Service;

import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;

@Service
public class CalculadoraService {

  // Construimos el manejo de excepciones
  public String parseAndEvaluate(String expression) {
    try {
      BigDecimal result = evaluate(expression);
      return "Result: " + result.setScale(2, RoundingMode.HALF_EVEN);
    } catch (IllegalArgumentException ex) {
      return "Error! " + ex.getMessage();
    } catch (ArithmeticException ex) {
      return "Error! Division entre cero invalido";
    } catch (Exception ex) {
      return "Error! Expresion invalida";
    }
  }

  // Evaluamos la expresion parentesis balanceados, operadores consecutivos y
  // numeros con no mas de 1 decimal
  public BigDecimal evaluate(String expression) {
    char[] elements = expression.toCharArray();

    Stack<BigDecimal> values = new Stack<>();
    Stack<Character> operators = new Stack<>();

    for (int i = 0; i < elements.length; i++) {
      if (elements[i] == ' ') {
        continue;
      }

      if (Character.isDigit(elements[i])) {
        StringBuilder sb = new StringBuilder();
        while (i < elements.length && (Character.isDigit(elements[i]) || elements[i] == '.')) {
          sb.append(elements[i++]);
        }
        BigDecimal number = new BigDecimal(sb.toString());
        if (!isValidDecimal(number)) {
          throw new IllegalArgumentException("El numero contiene mas de un decimal: " + number);
        }
        values.push(number);
        i--;
      } else if (elements[i] == '(') {
        operators.push(elements[i]);
      } else if (elements[i] == ')') {
        while (operators.peek() != '(') {
          values.push(execOperation(operators.pop(), values.pop(), values.pop()));
        }
        operators.pop(); // Pop '('
      } else if (elements[i] == '+' || elements[i] == '-' || elements[i] == '*' || elements[i] == '/') {
        while (!operators.isEmpty() && hasPrecedence(elements[i], operators.peek())) {
          values.push(execOperation(operators.pop(), values.pop(), values.pop()));
        }
        operators.push(elements[i]);
      }
    }

    while (!operators.isEmpty()) {
      values.push(execOperation(operators.pop(), values.pop(), values.pop()));
    }

    return values.pop();
  }

  private boolean hasPrecedence(char op1, char op2) {
    return (op2 != '(' && op2 != ')') && (precedence(op1) <= precedence(op2));
  }

  private int precedence(char op) {
    if (op == '+' || op == '-') {
      return 1;
    }
    if (op == '*' || op == '/') {
      return 2;
    }
    return 0;
  }

  // Ejecutamos las operaciones
  private BigDecimal execOperation(char operator, BigDecimal b, BigDecimal a) {
    switch (operator) {
      case '+':
        return a.add(b);
      case '-':
        return a.subtract(b);
      case '*':
        return a.multiply(b);
      case '/':
        if (b.compareTo(BigDecimal.ZERO) == 0) {
          throw new ArithmeticException("Division entre cero");
        }
        return a.divide(b, 2, RoundingMode.HALF_EVEN);
    }
    return BigDecimal.ZERO;
  }

  private boolean isValidDecimal(BigDecimal number) {
    String numberStr = number.stripTrailingZeros().toPlainString();
    return !numberStr.contains(".") || (numberStr.length() - numberStr.indexOf(".") - 1) <= 1;
  }
}
